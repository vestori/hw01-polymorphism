//
//  Person.h
//  HW01-Polymorphism
//
//  Created by Orlando Gotera on 9/23/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject

{
    NSString *personGender;
    NSString *personFirstName;
    NSString *personLastName;
}

@property (getter=getGender, setter=setGender: ) NSString *ender;
@property (getter=getFirstName, setter=setFirstName:) NSString *fName;
@property (getter=getLastName, setter=setLastName: ) NSString *lName;

-(id)initWithFirstName:(NSString *) firstName andWithLastname:(NSString *) lastName andWithGender:(NSString *) gender;

@end
