//
//  MDCPerson.h
//  HW01-Polymorphism
//
//  Created by Orlando Gotera on 9/23/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import "Person.h"

@interface MDCPerson : Person

{
    NSString *personCampus;
    NSString *personRole;
}

//constructor
-(id)initWithFirstName: (NSString *) firstName andWithLastName: (NSString *) lastName andWithGender: (NSString *) gender andWithCampus: (NSString *) campus andWithRole: (NSString *) role;

//setters
-(void) setCampus:(NSString *) campus;
-(void) setRole:(NSString *) role;

//getters
-(NSString *) getCampus;
-(NSString *) getRole;




@end
