//
//  MDCStudent.m
//  HW01-Polymorphism
//
//  Created by Orlando Gotera on 9/23/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import "MDCStudent.h"

@implementation MDCStudent

//constructor implemenation
-(id)initWithFirstName: (NSString *) firstName andWithLastName: (NSString *) lastName andWithGender: (NSString *) gender andWithCampus: (NSString *) campus andWithRole: (NSString *) role andWithMajor: (NSString *) major andWithClassification: (NSString *) classification {
    [super setFirstName:firstName];
    [super setLastName:lastName];
    [super setGender:gender];
    [super setCampus:campus];
    [super setRole:role];
    [self setMajor:major];
    [self setClassification:classification];
    return self;
    
}

//setters implementation
-(void) setMajor: (NSString *) major {
    studentMajor = major;
}
-(void) setClassification: (NSString *) classification {
    studentClassification = classification;
}

//getters implemenation
-(NSString *) getMajor {
    return studentMajor;
}

-(NSString *) getClassification {
    return studentClassification;
}

@end
