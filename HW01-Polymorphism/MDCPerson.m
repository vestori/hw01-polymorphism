//
//  MDCPerson.m
//  HW01-Polymorphism
//
//  Created by Orlando Gotera on 9/23/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import "MDCPerson.h"

@implementation MDCPerson

//construction implementation
-(id)initWithFirstName: (NSString *) firstName andWithLastName: (NSString *) lastName andWithGender: (NSString *) gender andWithCampus: (NSString *) campus andWithRole: (NSString *) role {
    [super setFirstName:firstName];
    [super setLastName:lastName];
    [super setGender:gender];
    [self setCampus:campus];
    [self setRole:role];
    return self;
}

//setters implemenation
-(void)setRole:(NSString *)role {
    personRole = role;
}

-(void)setCampus:(NSString *)campus {
    personCampus = campus;
}

//getters implemenation 
-(NSString *)getRole {
    return personRole;
}

-(NSString *)getCampus {
    return personCampus;
}



@end
