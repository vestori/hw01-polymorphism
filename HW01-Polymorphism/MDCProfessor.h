//
//  MDCProfessor.h
//  HW01-Polymorphism
//
//  Created by Orlando Gotera on 9/23/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import "MDCPerson.h"

@interface MDCProfessor : MDCPerson

{
    NSString *personTeachSpec;
    NSString *personDept;
}

//constructor
-(id)initWithFirstName: (NSString *) firstName andWithLastName: (NSString *) lastName andWithGender: (NSString *) gender andWithCampus: (NSString *) campus andWithRole: (NSString *) role andWithTeachSpec: (NSString *) teachSpec andWithDept: (NSString *) dept;

//setters
-(void) setTeachSpec: (NSString *) specialty;
-(void) setDepartment: (NSString *) department;

//getters
-(NSString *) getTeachSpec;
-(NSString *) getDepartment;

@end
