//
//  main.m
//  HW01-Polymorphism
//
//  Created by Orlando Gotera on 9/23/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#ifdef DEBUG
#define NSLog(FORMAT, ...) fprintf(stderr,"%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);
#else
#define NSLog(...) {}
#endif

#import <Foundation/Foundation.h>
#import "Person.h"
#import "MDCPerson.h"
#import "MDCProfessor.h"
#import "MDCStudent.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        //creating an array per object type to show that all fields work.
        NSMutableArray *myPersons = [[NSMutableArray alloc]init];
        NSMutableArray *myMDCPersons = [[NSMutableArray alloc]init];
        NSMutableArray *myStudents = [[NSMutableArray alloc]init];
        NSMutableArray *myProfessors = [[NSMutableArray alloc]init];
        
        //creating the objects per class, all created as Person
        Person *person1 = [[Person alloc]initWithFirstName:@"John" andWithLastname:@"Smith" andWithGender:@"Male"];
        
        Person *person2 = [[Person alloc]initWithFirstName:@"Michael" andWithLastname:@"Gonzalez" andWithGender:@"Male"];
        
        Person *mdcPerson1 = [[MDCPerson alloc]initWithFirstName:@"Thomas" andWithLastName:@"Martinez" andWithGender:@"Male" andWithCampus:@"Wolfson" andWithRole:@"Custodian"];
        
        Person *mdcPerson2 = [[MDCPerson alloc]initWithFirstName:@"Jennifer" andWithLastName:@"Smith" andWithGender:@"Female" andWithCampus:@"North" andWithRole:@"Security Guard"];
        
        Person *professor1 = [[MDCProfessor alloc]initWithFirstName:@"Pedro" andWithLastName:@"Diaz" andWithGender:@"Male" andWithCampus:@"Kendall" andWithRole:@"Professor" andWithTeachSpec:@"Mathematics" andWithDept:@"Math and Science"];
        
        Person *professor2 = [[MDCProfessor alloc]initWithFirstName:@"Jessica" andWithLastName:@"Romano" andWithGender:@"Female" andWithCampus:@"Hialeah" andWithRole:@"Professor" andWithTeachSpec:@"History" andWithDept:@"History Department"];
        
        
        Person *student1 = [[MDCStudent alloc]initWithFirstName:@"Amelie" andWithLastName:@"Diaz" andWithGender:@"Female" andWithCampus:@"North" andWithRole:@"Student" andWithMajor:@"Art History" andWithClassification:@"Freshman"];
        
        Person *student2 = [[MDCStudent alloc]initWithFirstName:@"Pamela" andWithLastName:@"Albizu" andWithGender:@"Female" andWithCampus:@"Hialeah" andWithRole:@"Student" andWithMajor:@"Health Services Administration" andWithClassification:@"Senior"];
        
        //adding objects to arrays
        [myPersons addObject:person1];
        [myPersons addObject:person2];
        
        [myMDCPersons addObject:mdcPerson1];
        [myMDCPersons addObject:mdcPerson2];
        
        [myProfessors addObject:professor1];
        [myProfessors addObject:professor2];
        
        [myStudents addObject:student1];
        [myStudents addObject:student2];
        
        //for loops to print all objects
        for (Person *aPerson in myPersons) {
            NSLog(@"\nFirst Name: %@ \nLast Name: %@ \nGender: %@", [aPerson getFirstName], [aPerson getLastName], [aPerson getGender]);
        }
        for (MDCPerson *anMdcPerson in myMDCPersons) {
            NSLog(@"\nFirst Name: %@ \nLast Name: %@ \nGender: %@ \nCampus: %@ \nRole: %@", [anMdcPerson getFirstName], [anMdcPerson getLastName], [anMdcPerson getGender], [anMdcPerson getCampus], [anMdcPerson getRole]);
        }
        for (MDCProfessor *anMdcProfessor in myProfessors) {
            NSLog(@"\nFirst Name: %@ \nLast Name: %@ \nGender: %@ \nCampus: %@ \nRole: %@ \nSpecialty: %@ \nDepartment: %@", [anMdcProfessor getFirstName], [anMdcProfessor getLastName], [anMdcProfessor getGender], [anMdcProfessor getCampus], [anMdcProfessor getRole], [anMdcProfessor getTeachSpec], [anMdcProfessor getDepartment]);
        }
        for (MDCStudent *aStudent in myStudents) {
            NSLog(@"\nFirst Name: %@ \nLast Name: %@ \nGender: %@ \nCampus: %@ \nRole: %@ \nMajor: %@ \nClassification: %@", [aStudent getFirstName], [aStudent getLastName], [aStudent getGender], [aStudent getCampus], [aStudent getRole], [aStudent getMajor], [aStudent getClassification]);
        }
        
    }
    return 0;
}
