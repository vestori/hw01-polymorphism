//
//  MDCProfessor.m
//  HW01-Polymorphism
//
//  Created by Orlando Gotera on 9/23/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import "MDCProfessor.h"

@implementation MDCProfessor

//constructor implementation
-(id)initWithFirstName: (NSString *) firstName andWithLastName: (NSString *) lastName andWithGender: (NSString *) gender andWithCampus: (NSString *) campus andWithRole: (NSString *) role andWithTeachSpec: (NSString *) teachSpec andWithDept: (NSString *) dept {
    [super setFirstName:firstName];
    [super setLastName:lastName];
    [super setGender:gender];
    [super setCampus:campus];
    [super setRole:role];
    [self setTeachSpec:teachSpec];
    [self setDepartment:dept];
    return self;
    
}

//setter implemenation
-(void)setTeachSpec:(NSString *)specialty {
    personTeachSpec = specialty;
}

-(void)setDepartment:(NSString *)department {
    personDept = department;
}

//getter implemenation
-(NSString *) getTeachSpec {
    return personTeachSpec;
}

-(NSString *) getDepartment {
    return personDept;
}

@end
