//
//  MDCStudent.h
//  HW01-Polymorphism
//
//  Created by Orlando Gotera on 9/23/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import "MDCPerson.h"

@interface MDCStudent : MDCPerson

{
    NSString *studentMajor;
    NSString *studentClassification;
}

//constructor
-(id)initWithFirstName: (NSString *) firstName andWithLastName: (NSString *) lastName andWithGender: (NSString *) gender andWithCampus: (NSString *) campus andWithRole: (NSString *) role andWithMajor: (NSString *) major andWithClassification: (NSString *) classification;

//setters
-(void) setMajor: (NSString *) major;
-(void) setClassification: (NSString *) classification;

//getters
-(NSString *) getMajor;
-(NSString *) getClassification;

@end
