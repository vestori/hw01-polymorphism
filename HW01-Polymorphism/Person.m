//
//  Person.m
//  HW01-Polymorphism
//
//  Created by Orlando Gotera on 9/23/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import "Person.h"

@implementation Person

-(id)initWithFirstName:(NSString *) firstName andWithLastname:(NSString *) lastName andWithGender:(NSString *) gender {
    [self setFirstName:firstName];
    [self setLastName:lastName];
    [self setGender:gender];
    return self;
}

@end
